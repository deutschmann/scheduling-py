def minimise_number_of_delayed_jobs(jobs):
    print("\n\nMinimising number of delayed jobs:")
    print("----------------------------------")

    # prepare data structure
    in_time = []
    late = []

    jobs_initially_sorted = list(jobs.items())

    # sort jobs by increasing due dates
    jobs_initially_sorted.sort(key=lambda x: x[1][1])

    passed_time = 0
    for job_id, (p_j, d_j) in jobs_initially_sorted:
        c_j = p_j + passed_time
        in_time.append(job_id)
        passed_time += p_j

        if c_j <= d_j:
            # we make it in time, all is good
            print("J" + str(job_id) + " in time")
            pass
        else:
            # we are delayed, so we move the longest running job to the end
            in_time_processing_times = list(map(lambda x: jobs[x][0], in_time))
            move_idx = in_time_processing_times.index(max(in_time_processing_times))
            late.append(in_time[move_idx])

            print("J" + str(job_id) + " too late → moving J" + str(in_time[move_idx]) + " to the end")

            del in_time[move_idx]
            passed_time -= in_time_processing_times[move_idx]

        print("Current order: " + str(in_time) + " ... " + str(late) + "\n")

    print("Done. Result:")

    jobs_order = in_time + late
    jobs_sorted = {}
    for job_id in jobs_order:
        jobs_sorted[job_id] = jobs[job_id]

    print_table(jobs, list(jobs_sorted.items()), jobs_order)


def minimise_maximum_tardiness(jobs):
    print("Minimising maximum tardiness:")
    print("-----------------------------")

    jobs_sorted = list(jobs.items())

    # just sort jobs by increasing due dates
    jobs_sorted.sort(key=lambda x: x[1][1])

    jobs_order = list(map(lambda x: x[0], jobs_sorted))

    print_table(jobs, jobs_sorted, jobs_order)


def print_table(jobs, jobs_sorted, jobs_order):
    passed_time = 0
    c = {}
    t = {}
    for job_id, (p_j, d_j) in jobs_sorted:
        c[job_id] = p_j + passed_time
        passed_time += p_j
        t[job_id] = max(0, c[job_id] - d_j)

    c_sorted = list(c.items())
    c_sorted.sort(key=lambda x: x[0])
    t_sorted = list(t.items())
    t_sorted.sort(key=lambda x: x[0])

    print("final job order = " + str(jobs_order))

    print("\nj   = " + str(list(jobs.keys())))
    print("p_j = " + str(list(map(lambda x: x[0], jobs.values()))))
    print("d_j = " + str(list(map(lambda x: x[1], jobs.values()))))
    print("c_j = " + str(list(map(lambda x: x[1], c_sorted))))
    print("t_j = " + str(list(map(lambda x: x[1], t_sorted))))

    print("\nmaximum tardiness = " + str(max(t.values())))
    print("number of delayed jobs = " + str(len(list(filter(lambda x: x > 0, t.values())))))


if __name__ == '__main__':
    # key = job_id
    # value = (processing time p_j, due date d_j)
    jobs_ex = {
        1: (6, 7),
        2: (8, 13),
        3: (8, 15),
        4: (5, 11),
        5: (6, 12),
        6: (4, 18),
        7: (2, 10),
        8: (6, 12)
    }

    # key = job_id
    # value = (processing time p_j, due date d_j)
    jobs_slides = {
        1: (8, 9),
        2: (4, 10),
        3: (4, 12),
        4: (8, 14),
        5: (6, 16)
    }

    minimise_maximum_tardiness(jobs_ex)
    minimise_number_of_delayed_jobs(jobs_ex)
