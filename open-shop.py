def open_shop(jobs, new_algorithm=True):
    # 0. Preparation of data structures
    machine_orders = {
        1: [],
        2: []
    }
    machine_availability = {
        1: 0,
        2: 0
    }
    job_times = {
        1: {},
        2: {}
    }

    job_candidates = list(jobs.items())

    # 1. Initialization
    p_m1 = list(map(lambda x: x[1][0], jobs.items()))
    p_m2 = list(map(lambda x: x[1][1], jobs.items()))

    max_m1 = max(p_m1)
    max_m2 = max(p_m2)

    # slide rule: find longest running task and plan it on the other machine
    # LAPT: find longest running task and plan it on this very machine
    if (max_m1 > max_m2) == new_algorithm:
        job_id = list(jobs.keys())[p_m1.index(max_m1)]
        machine_orders[2].append(job_id)
        p_job = jobs[job_id][1]
        machine_availability[2] = p_job
        job_times[2][job_id] = (0, p_job)
        print("J" + str(job_id) + " → M2")
    else:
        job_id = list(jobs.keys())[p_m2.index(max_m2)]
        machine_orders[1].append(job_id)
        p_job = jobs[job_id][0]
        machine_availability[1] = p_job
        job_times[1][job_id] = (0, p_job)
        print("J" + str(job_id) + " → M1")

    job_idx = list(jobs.keys()).index(job_id)
    del job_candidates[job_idx]

    # 2. Iteration
    while len(job_candidates) > 0:
        # plan jobs on earliest available machines
        earliest_available_machine = min(machine_availability.items(), key=lambda x: x[1])[0]
        other_machine = 2 if earliest_available_machine == 1 else 1

        # slide rule: just take them by job_id
        # LAPT: start with jobs that take longest on the other machine
        if not new_algorithm:
            job_candidates.sort(key=lambda x: x[1][other_machine - 1], reverse=True)

        for idx, (j, _) in enumerate(job_candidates):
            if j not in machine_orders[earliest_available_machine]:
                machine_orders[earliest_available_machine].append(j)
                machine_availability[earliest_available_machine] += jobs[j][earliest_available_machine - 1]
                print("J" + str(j) + " → M" + str(earliest_available_machine))
                del job_candidates[idx]
                break

    # 3. Fill up
    m1_temp = machine_orders[1].copy()
    machine_orders[1] += machine_orders[2]
    machine_orders[2] += m1_temp

    print("\nM1 schedule: " + str(machine_orders[1]))
    print("M2 schedule: " + str(machine_orders[2]))

    m1_duration = sum(map(lambda x: x[0], jobs.values()))
    m2_duration = sum(map(lambda x: x[1], jobs.values()))

    likely_duration = max(m1_duration, m2_duration)
    print("\nLikely duration: " + str(likely_duration))
    print("WARNING: might not be correct if relatively very long tasks are in one job, verify using Gantt")


if __name__ == '__main__':
    '''
    Open-Shop-Problem:
    - all jobs need to processed on every machine
    - the order does not matter
    
    - uses LAPT rule (Largest Alternate Processing Time) or slide rule
    -- new_algortihm = true <=> slide rule
    -- new_algortihm = false <=> LAPT
    - minimise total processing time (C_max)
    - algorithm works only for two machines
    '''

    # key = job_id, value = (processing_time_M1, processing_time_M2)
    jobs_from_slides = {
        1: (4, 3),
        2: (2, 3),
        3: (12, 7),
        4: (9, 5),
        5: (3, 4),
        6: (5, 4),
        7: (5, 3),
        8: (2, 4)
    }

    jobs_from_ex = {
        1: (5, 3),
        2: (4, 3),
        3: (7, 6),
        4: (6, 5),
        5: (3, 2),
        6: (7, 3),
        7: (5, 3),
        8: (2, 4)
    }

    jobs_from_homework = {
        1: (2, 18),
        2: (1, 10),
        3: (9, 14),
        4: (6, 17),
        5: (16, 11),
        6: (12, 8),
        7: (5, 13)
    }

    jobs_experiment = {
        1: (40, 40),
        2: (2, 3),
        3: (12, 7),
        4: (9, 5),
        5: (3, 4),
        6: (5, 4),
        7: (5, 3),
        8: (2, 4)
    }

    jobs_from_homework_0 = {
        1: (19, 3),
        2: (6, 4),
        3: (14, 7),
        4: (15, 12),
        5: (13, 18),
        6: (11, 5),
        7: (1, 9)
    }

    open_shop(jobs_from_ex, True)
