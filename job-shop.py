import copy
import sys
from collections import defaultdict


# taken from https://stackoverflow.com/a/21977805/1318794
def transpose(dct):
    d = defaultdict(dict)
    for key1, inner in dct.items():
        for key2, value in inner.items():
            d[key2][key1] = value
    return d


def pick_candidate(j_candidates, p, rule='lpt'):
    if len(j_candidates) == 0:
        raise RuntimeError("No candidates!")

    processing_times = list(map(lambda x: sum(x[1].values()), transpose(p).items()))
    j_candidate_processing_times = list(map(lambda j: processing_times[j - 1], j_candidates))
    if rule is 'lpt':
        return j_candidates[j_candidate_processing_times.index(max(j_candidate_processing_times))]
    elif rule is 'spt':
        return j_candidates[j_candidate_processing_times.index(min(j_candidate_processing_times))]
    else:
        raise ValueError("Unknown priority rule")


def giffler_thompson(job_orders, p, rule='lpt'):
    num_jobs = len(job_orders.keys())
    num_machines = len(list(job_orders.items())[0][1])

    # prepare data structures
    machine_orders = {}
    Z = {}
    r = {}
    for i in range(1, num_machines + 1):
        machine_orders[i] = []
        Z[i] = 0

    for j in range(1, num_jobs + 1):
        r[j] = 0

    open_tasks = copy.deepcopy(job_orders)

    # run algorithm
    iteration = 1
    while max(map(lambda x: len(x[1]), open_tasks.items())) > 0:
        print("\niter " + str(iteration))
        print_state(p, open_tasks, num_jobs, num_machines)
        print("r=" + str(list(r.values())) + " Z=" + str(list(Z.values())))

        # 1. compute earliest task completion times and pick minimum for earliest completion time
        #    C_start and derive ideal machine i_star
        C_star = sys.maxsize
        i_star = None
        j_candidates = {}
        for j in range(1, num_jobs + 1):
            if len(open_tasks[j]) == 0:
                # this job is completed
                continue
            i = open_tasks[j][0]  # next up machine for job
            C_ij = max(Z[i], r[j]) + p[i][j]
            if C_ij <= C_star:
                C_star = C_ij
                i_star = i
            if i not in j_candidates:
                j_candidates[i] = []
            j_candidates[i].append(j)
        # look only at candidates for chosen machine
        j_candidates = j_candidates[i_star]

        # filter to those candidates that are available
        j_candidates = list(filter(lambda x: r[x] < C_star, j_candidates))

        # 2. pick ideal task j_star based on priority rule
        j_star = pick_candidate(j_candidates, p, rule)

        # 3. Update result list, Z_i and r_ij
        Z[i_star] = max(Z[i_star], r[j_star]) + p[i_star][j_star]
        r[j_star] = max(Z[i_star], r[j_star] + p[i_star][j_star])

        machine_orders[i_star].append(j_star)

        assert open_tasks[j_star][0] == i_star
        del open_tasks[j_star][0]

        print("C*=" + str(C_star) + " i*=" + str(i_star) + " j*=" + str(j_star))
        print(str(iteration) + ". Iteration: J" + str(j_star) + " on M" + str(i_star))

        iteration += 1

    assert max(Z.values()) == max(r.values())

    print("\nGiffler & Thompson result:")
    print("----------------------------")
    print("final machine order: ")
    print(*machine_orders.values(), sep="\n")
    print("\nDuration = last task finishes at = " + str(max(Z.values()) ))


def print_state(p, open_tasks, num_jobs, num_machines):
    state_map = [[None for y in range(num_jobs)] for x in range(num_machines)]

    for j in range(0, num_jobs):
        for k in range(0, num_machines):
            if k + 1 not in open_tasks[j + 1]:
                state_map[k][j] = 'x'
            else:
                curr_p = p[k + 1][j + 1]
                next_up = open_tasks[j + 1][0]
                if next_up == k + 1:
                    state_map[k][j] = "[" + str(curr_p) + "]"
                else:
                    state_map[k][j] = str(curr_p)

    print_map = list(map(lambda x: '| ' + '|'.join(list(map(lambda y: y.rjust(5),x))) + ' |', state_map))

    print("---------------------------")
    print(*print_map, sep="\n")
    print("---------------------------")


if __name__ == '__main__':
    '''
    Job-Shop-Problem:
    - jobs have different orders on machines
    - jobs have differen processing times on machines
    - heuristic works for an arbitrary number of machines 
    '''

    # jobs_homework = [  # task = (machine_id, processing_time).
    #     [(1,  6), (3, 16), (2, 11)],  # Job1
    #     [(3,  4), (2, 18), (1,  7)],   # Job2
    #     [(3, 13), (1, 15), (2,  9)],  # Job3
    #     [(2, 13), (3,  1), (3, 13)]   # Job4
    # ]

    # key=job_id,
    # value=list of machine_ids
    job_orders_slides = {
        1: [1, 2, 3, 4],
        2: [2, 1, 3, 4],
        3: [4, 3, 2, 1],
        4: [3, 4, 2, 1]
    }

    # key=machine_id
    # value=dict of key=job_id value=processing time of job j on machine i
    job_processing_times_slides = {
        1: {
            1: 5, 2: 7, 3: 1, 4: 2
        },
        2: {
            1: 3, 2: 4, 3: 6, 4: 1
        },
        3: {
            1: 3, 2: 8, 3: 5, 4: 4
        },
        4: {
            1: 2, 2: 6, 3: 3, 4: 7
        }
    }

    # giffler_thompson(job_orders_slides, job_processing_times_slides, rule='lpt')

    # ----------------------------------------------------------------------
    # ----------------------------------------------------------------------

    # key=job_id,
    # value=list of machine_ids
    job_orders_homework = {
        1: [1, 3, 2],
        2: [3, 2, 1],
        3: [3, 1, 2],
        4: [2, 3, 1]
    }

    # key=machine_id
    # value=dict of key=job_id value=processing time of job j on machine i
    job_processing_times_homework = {
        1: {
            1: 6, 2: 7, 3: 15, 4: 17
        },
        2: {
            1: 11, 2: 18, 3: 9, 4: 13
        },
        3: {
            1: 16, 2: 4, 3: 14, 4: 1
        }
    }

    giffler_thompson(job_orders_homework, job_processing_times_homework, rule='spt')

    # ----------------------------------------------------------------------
    # ----------------------------------------------------------------------

    # key=job_id,
    # value=list of machine_ids
    job_orders_homework_0 = {
        1: [3, 2, 1],
        2: [1, 2, 3],
        3: [2, 3, 1],
        4: [2, 3, 1]
    }

    # key=machine_id
    # value=dict of key=job_id value=processing time of job j on machine i
    job_processing_times_homework_0 = {
        1: {
            1: 14,
            2: 8,
            3: 17,
            4: 11
        },
        2: {
            1: 16,
            2: 10,
            3: 4,
            4: 13
        },
        3: {
            1: 12,
            2: 7,
            3: 2,
            4: 5
        }
    }

    # giffler_thompson(job_orders_homework_0, job_processing_times_homework_0, rule='spt')

    # ----------------------------------------------------------------------
    # ----------------------------------------------------------------------

    # key=job_id,
    # value=list of machine_ids
    job_orders_ex = {
        1: [1, 2, 3],
        2: [2, 1, 3],
        3: [1, 3, 2]
    }

    # key=machine_id
    # value=dict of key=job_id value=processing time of job j on machine i
    job_processing_times_ex = {
        1: {
            1: 11,
            2: 6,
            3: 3,
        },
        2: {
            1: 9,
            2: 8,
            3: 4
        },
        3: {
            1: 4,
            2: 8,
            3: 3
        }
    }

    # giffler_thompson(job_orders_ex, job_processing_times_ex, rule='lpt')
