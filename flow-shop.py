def johnson(jobs):
    l = []
    r = []

    open_jobs = list(jobs.items())

    # compute order
    while len(open_jobs) > 0:
        # 1. find whether job is faster on M1 or M2
        min_m1 = min(map(lambda x: x[1][0], open_jobs))
        min_m2 = min(map(lambda x: x[1][1], open_jobs))

        if min_m1 <= min_m2:
            job_idx = list(map(lambda x: x[1][0], open_jobs)).index(min_m1)
        else:
            job_idx = list(map(lambda x: x[1][1], open_jobs)).index(min_m2)

        job_id = open_jobs[job_idx][0]
        (p_m1, p_m2) = open_jobs[job_idx][1]

        # 2. if on M1 -> append to L, otherwise prepend to R
        if p_m1 < p_m2:
            l.append(job_id)
        else:
            r.insert(0, job_id)

        del open_jobs[job_idx]

        print("L=" + str(l) + " R=" + str(r))

    job_order = l + r

    # compute completion times

    # - compute machine 1 times
    job_times_m1 = {}
    for job_idx, job_id in enumerate(job_order):
        p_m1 = jobs[job_id][0]
        if job_idx == 0:
            job_times_m1[job_id] = (0, p_m1)
        else:
            prev_job_finishes = job_times_m1[job_order[job_idx - 1]][1]
            job_times_m1[job_id] = (prev_job_finishes, prev_job_finishes + p_m1)

    # - compute machine 2 times
    job_times_m2 = {}
    for job_idx, job_id in enumerate(job_order):
        p_m2 = jobs[job_id][1]
        finish_on_m1 = job_times_m1[job_id][1]
        if job_idx == 0:
            job_times_m2[job_id] = (finish_on_m1, finish_on_m1 + p_m2)
        else:
            prev_job_finishes = job_times_m2[job_order[job_idx - 1]][1]
            # we can only start after m1 is than and we are done with prev job
            start = max(finish_on_m1, prev_job_finishes)
            job_times_m2[job_id] = (start, start + p_m2)

    C_max = list(job_times_m2.items())[-1][1][1]

    print("\nJohnson result:")
    print("---------------")

    print("Final job order: " + str(job_order))

    print("\nM1 Job start and completion times: ")
    print(*job_times_m1.items(), sep="\n")

    print("\nM2 Job start and completion times: ")
    print(*job_times_m2.items(), sep="\n")

    print("\nTotal processing time C_max = " + str (C_max))


if __name__ == '__main__':
    '''
    Flow-Shop-Problem:
    - all jobs are processed first on M1, then on M2
    - jobs have different processing times on the respective machines
    
    - minimise total processing time (C_max)
    - algorithm works only for 2 machines
    '''

    # key = job_id, value = (processing_time_M1, processing_time_M2)
    jobs_from_slides = {
        1: (4, 8),
        2: (3, 3),
        3: (3, 4),
        4: (1, 4),
        5: (8, 7)
    }

    jobs_homework = {
        1: (18, 4),
        2: (5, 10),
        3: (9, 17),
        4: (13, 15),
        5: (8, 7),
        6: (2, 1)
    }

    jobs_homework_0 = {
        1: (16, 7),
        2: (3, 10),
        3: (2, 15),
        4: (9, 11),
        5: (12, 19),
        6: (14, 6)
    }

    jobs_from_ex = {
        1: (5, 3),
        2: (4, 3),
        3: (7, 6),
        4: (6, 5),
        5: (3, 2),
        6: (7, 3),
        7: (5, 3),
        8: (2, 4)
    }

    johnson(jobs_from_ex)
