def minimal_makespan(jobs, m):
    # uses approximation algorithm with LPT-rule

    jobs_sorted = list(jobs.items())

    # 1. sort by decreasing processing time
    jobs_sorted.sort(key=lambda x: x[1], reverse=True)  # LPT -> reverse=True

    m_jobs = [[] for x in range(m)]
    m_p = [[] for x in range(m)]

    for job, p in jobs_sorted:
        # 2. add job to machine with currently lowest processing time
        current_processing_times = list(map(lambda x: sum(x), m_p))
        m_idx = current_processing_times.index(min(current_processing_times))

        m_jobs[m_idx].append(job)
        m_p[m_idx].append(p)

    print("Minimal Makespan result")
    print("-----------------------")

    m_finishes = []

    for m_i in range(m):
        print("M" + str(m_i + 1) + " jobs: " + str(m_jobs[m_i]))

        m_i_finishes = sum(m_p[m_i])
        print("M" + str(m_i + 1) + " finishes at: " + str(m_i_finishes))
        m_finishes.append(m_i_finishes)

    print("Makespan: " + str(max(m_finishes)))


def minimal_completion_times(jobs, m):
    # uses SPT rule

    jobs_sorted = list(jobs.items())

    # 1. sort jobs by increasing processing time
    jobs_sorted.sort(key=lambda x: x[1], reverse=False)  # SPT -> reverse=False

    m_jobs = [[] for x in range(m)]
    m_p = [[] for x in range(m)]

    for job, p in jobs_sorted:
        # 2. add job to machine with currently lowest processing time
        current_processing_times = list(map(lambda x: sum(x), m_p))
        m_idx = current_processing_times.index(min(current_processing_times))

        m_jobs[m_idx].append(job)
        m_p[m_idx].append(p)

    print("Minimal Completion Time Result")
    print("------------------------------")

    for m_i in range(m):
        print("M" + str(m_i + 1) + " jobs: " + str(m_jobs[m_i]))
        print("M" + str(m_i + 1) + " finishes at: " + str(sum(m_p[m_i])))

    # compute completion times
    c = {}
    for m_i in range(m):
        current_jobs = m_jobs[m_i]
        current_processing_times = m_p[m_i]
        for i, job_id in enumerate(current_jobs):
            c[job_id] = sum(current_processing_times[:i + 1])

    c_sorted = list(c.items())
    c_sorted.sort(key=lambda x: x[0])
    print("\nJob completion times:")
    print(*c_sorted, sep="\n")

    sum_c_j = sum(map(lambda x: x[1], c_sorted))
    print("Sum of completion times: " + str(sum_c_j))


if __name__ == '__main__':
    # key = job_id, value = processing_time
    jobs_slides_makespan = {
        1: 8,
        2: 6,
        3: 6,
        4: 5,
        5: 4,
        6: 3,
        7: 3,
        8: 1
    }
    m_slides_makespan = 2

    # minimal_makespan(jobs_slides_makespan, m_slides_makespan)

    # key = job_id, value = processing_time
    jobs_slides_completion = {
        1: 9,
        2: 8,
        3: 6,
        4: 7,
        5: 4,
        6: 3,
        7: 3,
        8: 1,
        9: 4,
        10: 2,
        11: 5
    }

    m_slides_completion = 3

    # minimal_completion_times(jobs_slides_completion, m_slides_completion)

    jobs_ex_2a = {
        1: 8,
        2: 6,
        3: 12,
        4: 13,
        5: 9,
        6: 14,
        7: 12,
        8: 11,
        9: 13,
        10: 9,
        11: 12
    }

    m_ex_2a = 3

    # minimal_makespan(jobs_ex_2a, m_ex_2a)

    jobs_ex_2b = {
        1: 3,
        2: 3,
        3: 2,
        4: 2,
        5: 2
    }

    m_ex_2b = 2

    # minimal_makespan(jobs_ex_2b, m_ex_2b)

    jobs_ex_3 = {
        1: 12,
        2: 4,
        3: 5,
        4: 6,
        5: 12,
        6: 11,
        7: 12,
        8: 6,
        9: 7,
        10: 16,
        11: 13,
        12: 15,
        13: 13,
        14: 9,
        15: 12
    }

    m_ex_3 = 3

    minimal_completion_times(jobs_ex_3, m_ex_3)
